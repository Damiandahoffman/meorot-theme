<?php

add_action( 'customize_register', 'meorot_customize_register' );
function meorot_customize_register($wp_customize)
{
   $wp_customize->add_section('meorot_theme_setting', array(
        'title'    => __('הגדרות כנס מאורות', 'meorot'),
        'priority' => 10,
    ));

	$wp_customize->add_setting('meorot_theme_options[copyright]', array(
        'default'        => 'כל הזכויות שמורות לעמותות המארגנות',
        'capability'     => 'edit_theme_options',
		'type' => 'theme_mod',
    ));
	$wp_customize->add_control('meorot_theme_copyright', array(
        'label'      => __('זכויות יוצרים', 'copyright'),
        'section'    => 'meorot_theme_setting',
        'settings'   => 'meorot_theme_options[copyright]',
    ));
	
	
	$wp_customize->add_setting('meorot_theme_options[color][article_background_color]', array(
        'capability'     => 'edit_theme_options',
		'type' => 'theme_mod',
    ));
	$wp_customize->add_control(new WP_Customize_Color_Control( $wp_customize, 'meorot_theme-article_background_color',
    array(
        'label'    => __( 'רקע פוסט', 'Post Background' ),
        'section'  => 'meorot_theme_setting',
        'settings' => 'meorot_theme_options[color][article_background_color]',
    )
	));
	
	$wp_customize->add_setting('meorot_theme_options[color][meorot_link_color]', array(
        'capability'     => 'edit_theme_options',
		'type' => 'theme_mod',
    ));
	$wp_customize->add_control(new WP_Customize_Color_Control( $wp_customize, 'meorot_theme-meorot_link_color',
    array(
        'label'    => __( 'צבע קישור', 'Link Color' ),
        'section'  => 'meorot_theme_setting',
        'settings' => 'meorot_theme_options[color][meorot_link_color]',
    )
	));
	
	$wp_customize->add_setting('meorot_theme_options[color][meorot_global_color_primary]', array(
        'capability'     => 'edit_theme_options',
		'type' 			 => 'theme_mod',
    ));
	$wp_customize->add_control(new WP_Customize_Color_Control( $wp_customize, 'meorot_theme-meorot_global_color_primary',
    array(
        'label'    => __( 'צבע טקסט', 'Text Color' ),
        'section'  => 'meorot_theme_setting',
        'settings' => 'meorot_theme_options[color][meorot_global_color_primary]',
    )
	));
	
	$wp_customize->add_setting('meorot_theme_options[color][meorot_selected_link_color]', array(
        'capability'     => 'edit_theme_options',
		'type' 			 => 'theme_mod',
    ));
	$wp_customize->add_control(new WP_Customize_Color_Control( $wp_customize, 'meorot_theme-meorot_selected_link_color',
    array(
        'label'    => __( 'צבע קישור נבחר', 'Text Color' ),
        'section'  => 'meorot_theme_setting',
        'settings' => 'meorot_theme_options[color][meorot_selected_link_color]',
    )
	));
	
	
	$wp_customize->add_setting('meorot_theme_options[sidebar_display]', array(
	'default'        => '',
	'capability'     => 'edit_theme_options',
	'type' => 'theme_mod',
	));
	$wp_customize->add_control('meorot_theme_sidebar_display', array(
        'label'      => __('הצג סרגל צד', 'sidebar_display'),
        'section'    => 'meorot_theme_setting',
        'settings'   => 'meorot_theme_options[sidebar_display]',
		'type' 		 => 'checkbox',
    ));
	
	$wp_customize->add_setting('meorot_theme_options[seperator_image]', array(
        'capability'     => 'edit_theme_options',
		'type' => 'theme_mod',
    ));
	$wp_customize->add_control(new WP_Customize_Image_Control( $wp_customize, 'meorot_theme-seperator_image',
    array(
        'label'    => __( 'תמונת מפריד', 'Seperator Image' ),
        'section'  => 'meorot_theme_setting',
        'settings' => 'meorot_theme_options[seperator_image]',
    )
	));
	


	
	$wp_customize->add_setting('meorot_theme_options[js]', array(
        'default'        => '',
        'capability'     => 'edit_theme_options',
		'type' => 'theme_mod',
    ));
	$wp_customize->add_control('meorot_theme_js_script', array(
        'label'      => __('סקריפט נוסף', 'js_script'),
        'section'    => 'meorot_theme_setting',
        'settings'   => 'meorot_theme_options[js]',
		'type' 		 => 'textarea',
    ));
}



function meorot_add_copyright()
{
$theme_options = get_theme_mod('meorot_theme_options');
print	'<script defer>(jQuery)(document).ready(function() 
	{
	if ( p = document.querySelector(".powered-by"))
	   {
			p.innerText = "'.$theme_options['copyright'].'";
	   }
	});</script>';
}

function meorot_set_customizer_style()
{
	$theme_options = get_theme_mod('meorot_theme_options');
	$colors = $theme_options['color'];
	print	'<style> :root {';
	print 	"--meorot-article-background-color:$colors[article_background_color];";
	print 	"--meorot-link-color:$colors[meorot_link_color];";
	print	"--meorot-seperator-image: url('$theme_options[seperator_image]');";
	print 	"--meorot-global-color-primary:$colors[meorot_global_color_primary];";
	print 	"--entry-header-color:$colors[meorot_global_color_primary];";
	print 	"--selected-link-color:$colors[meorot_selected_link_color];";
	print	'}';
	print $theme_options['sidebar_display'];
	if ($theme_options['sidebar_display'] == false) 
		print '#sidebar {display:none}';
	print	'</style>';
}

add_action( 'wp_footer', 'meorot_set_customizer_style' );
add_action( 'wp_footer', 'meorot_add_copyright' );

function meorot_add_theme_js()
{
	$theme_options = get_theme_mod('meorot_theme_options');	
	if (isset($theme_options['js']))
		print '<script>'.$theme_options['js'].'</script>';
}
add_action( 'wp_head', 'meorot_add_theme_js' );


?>