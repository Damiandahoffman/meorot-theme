<?php
/*
Template Name: יומן התוכן
*/
get_header();
while ( have_posts() ) :
	the_post();
	get_template_part( '/template-parts/content/content-page' );
	$entries = get_posts(array('post_type' => "diary",
			'orderby' => 'diary_id',
			'order' => 'ASC',
			'posts_per_page' => -1,
			'fields' => 'ids'));
				
	
	echo '<div id="entries">';
	echo "<div style='font-weight:bold'>לגישה מהירה</div>";
	echo '<ul class="entriesList">';
		foreach ($entries as $entry):
			$title = get_the_title($entry);
			echo "<li><a href='#diaryEntry_$entry' class='diaryDateLink'>$title</a></li>";
		endforeach;
	echo '</ul>';
		foreach ($entries as $entry):
			$title = get_the_title($entry);
			$text = (get_the_content("",false,$entry));
			echo "<div class='diaryDate' id='diaryEntry_$entry'>$title</div>";
			echo "<div class='diaryText'>$text</div>";
			echo "<hr>";
		endforeach;
	echo '</div>';
				
				
endwhile; // End of the loop.
get_sidebar('sidebar-1');
get_footer();
?>
<script defer>
(jQuery)(".entry-content").append((jQuery)("#entries").html());
(jQuery)("#entries").css("display","none");
</script>
