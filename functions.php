<?php

require_once( get_stylesheet_directory() . '/widget-diary.php');
require_once( get_stylesheet_directory() . '/widget-stories.php');
require_once( get_stylesheet_directory() . '/meorot-customizer.php');

function wp_cod_replace_jquery() {
	wp_enqueue_script('jquery');
}
add_action('init', 'wp_cod_replace_jquery');

// This is a persistent bastard - but we need it for the sogo plugin :(
function tn_dequeue_font_awesome_style() {
      wp_dequeue_style( 'fontawsome' );
      wp_deregister_style( 'fontawsome' );
}
//add_action( 'wp_enqueue_scripts', 'tn_dequeue_font_awesome_style', 9999 );
add_action( 'wp_head', 'tn_dequeue_font_awesome_style', 9999 );
add_action( 'wp_enqueue_scripts', 'child_enqueue_styles',99);
function child_enqueue_styles() 
{
    $parent_style = 'Parent';
    wp_enqueue_style( $parent_style, 			get_template_directory_uri() 	. '/style-rtl.css' );
	wp_enqueue_style( 'meorot2022', 			get_stylesheet_directory_uri() 	. '/style.css' );
	wp_enqueue_style( 'meorot2022_customizer',	get_stylesheet_directory_uri() 	. '/meorot-customizer.css');
}

if ( get_stylesheet() !== get_template() ) 
{
    add_filter( 'pre_update_option_theme_mods_' . get_stylesheet(), function ( $value, $old_value ) {
         update_option( 'theme_mods_' . get_template(), $value );
         return $old_value; // prevent update to child theme mods
    }, 10, 2 );
    add_filter( 'pre_option_theme_mods_' . get_stylesheet(), function ( $default ) {
        return get_option( 'theme_mods_' . get_template(), $default );
    } );
}
add_action( 'wp_head', 'meorot_add_header_link', 9999 );
function meorot_add_header_link()
{
	$url = home_url();
	print '<script>(jQuery)(document).ready(function(){
		  	(jQuery)(".custom-logo-link").wrap("<a href='.$url.'></a>");
		  });</script>';
}

function meorot_custom_types_init() {
    $args = array( 'public' => true, 'label' => 'יומן התוכן', 'menu_position' => 5 );
    register_post_type( 'diary', $args );
    $args = array( 'public' => true, 'label' => 'פרויקט סיפורים', 'menu_position' => 5, 'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ) );
    register_post_type( 'stories_project', $args );
}
add_action( 'init', 'meorot_custom_types_init' );


add_action('after_setup_theme', 'meorot_create_pages'); 
function meorot_create_pages()
{
	if (!($page = get_page_by_title( 'יומן התוכן' ))) 
	{
		$page = new stdClass();
        $page->ID = wp_insert_post(array('post_title' => "יומן התוכן",'post_name' =>"diary",'post_content' => "",'post_status' => "publish",'post_type' => 'page'));
        update_post_meta($page->ID, "_wp_page_template", "template-diary.php");
    }
    if (!($page = get_page_by_title( 'פרויקט הסיפורים' )))
	{
		$page = new stdClass();
        $page->ID = wp_insert_post(array('post_title' => "פרויקט הסיפורים",'post_name' =>"stories",'post_content' => "",'post_status' => "publish",'post_type' => 'page'));
        update_post_meta($page->ID, "_wp_page_template", "template-project-page.php");
    }
	if (!($page = get_page_by_title( 'על הכנס' )))
	{
		$page = new stdClass();
        $page->ID = wp_insert_post(array('post_title' => "על הכנס",'post_name' =>"main_page",'post_content' => "",'post_status' => "publish",'post_type' => 'page'));
    }
}
function meorot_register_sidebar()
{
	register_sidebar( array(
        'name'          => __( 'Primary Sidebar', 'meorot2022' ),
        'id'            => 'sidebar-1',
		'description' => __( 'Content that will go below post pages.' ),
                        'class' => 'widget-class',
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h2 class="widgettitle">',
		'after_title' => '</h2>',
    ) );
}
add_action( 'widgets_init', 'meorot_register_sidebar' );

add_action('after_setup_theme', 'meorot_set_homepage',20);
function meorot_set_homepage()
{
	$homepage = get_page_by_title( 'על הכנס' );
	if ( $homepage )
	{
		if ($homepage->ID != get_option('page_on_front'))
		{
			update_option( 'page_on_front', $homepage->ID );
			update_option( 'show_on_front', 'page' );
		}
	}
}

function meorot_edit_content( $content ) {
	global $post;
	// only edit specific post types
	$types = array( 'stories_project' );
	$page = get_page_by_title( 'פרויקט הסיפורים' );
	if ( $post && in_array( $post->post_type, $types, true ) ) 
	{
		$linkBack = '<div><a href="' . get_permalink($page->ID) . '"> בחזרה לפרויקט הסיפורים </a></div>';
		$content = $content . $linkBack;
	}
	return $content;
}

 add_filter( 'the_content', 'meorot_edit_content', -10 );

function meorot_sidebar_resize()
{
	print "<script defer>";
	print "if (parseInt((jQuery)('article.page').css('height')) > parseInt((jQuery)('#sidebar').css('height')))";
	print "{(jQuery)('#sidebar').css('height',(jQuery)('article.page').css('height'));}";
	print "</script>";
}
add_action( 'wp_footer', 'meorot_sidebar_resize');


add_filter( 'gettext', 'wpdocs_translate_text', 10, 3 );
function wpdocs_translate_text( $translated_text, $untranslated_text, $domain ) {
	//if ( 'twentytwentyone' !== $domain ) {
	//	return $translated_text;
	//}
	
	if ($untranslated_text == 'Menu')
	{
		return "תפריט";
	}
	if ($untranslated_text == 'Close')
	{
		return "סגירה";
	}
	if (empty($translated_text))
	{
		return $untranslated_text;
	}
	return $translated_text;

	// Now do your checking for your string within the 'blah' domain
}

?>