<?php 

// Creating the widget 
class stories_widget extends WP_Widget {

function __construct() {
	parent::__construct(
		// Base ID of your widget
		'stories_widget', 
		// Widget name will appear in UI
		__('Stories Widget', 'diary_widget_domain'), 
		// Widget description
		array( 'description' => __( 'Last story added for the side bar', 'diary_widget_domain' ), ) 
	);
}

// Creating widget front-end
// This is where the action happens
public function widget( $args, $instance ) {
	$title = apply_filters( 'widget_title', $instance['title'] );
	// before and after widget arguments are defined by themes
	echo $args['before_widget'];
	if ( ! empty( $title ) )
		echo $args['before_title'] . '<a href="'.get_permalink($instance[ 'page' ]).'">' . $title . '</a>' . $args['after_title'];
	
	// This is where you run the code and display the output
	
	$this->put_last_entry($instance['page']);

echo $args['after_widget'];
}

public function put_last_entry($story_id_page) {
	// get last story published
			$sql = "SELECT ID FROM ".$wpdb->prefix."posts WHERE post_type='stories_project' AND post_status='publish' ORDER BY menu_order";
			$stories = get_posts(array(
					'post_type' => 'stories_project',
					'post_status' => 'publish',
					'numberposts' => 1,
					'order_by'=>'menu_order',
					'order'    => 'DESC',
					'fields'   => 'ids',
				));
			if(!empty($stories)):
				$story_id = $stories[0];
				$randStory =  get_post($story_id );
				$title = strip_tags( $randStory->post_title );
				$link = get_permalink($story_id);
				print "<div class='storyProjectWrapper'>";
				print "<p class = 'widget-post-title'><a href='$link' class='widgetEntryTitle'>$title</a></p>";
				print "<a href='$link' class='widgetEntryText'>";
				if( !empty( $randStory->post_excerpt ) ) 
				{
					echo mb_substr( strip_tags( $randStory->post_excerpt, "<br><br/>" ), 0, 180 );
				}
				else 
				{
					echo mb_substr( strip_tags( $randStory->post_content, "<br><br/>" ), 0, 180 )."...";
				}
				print "</a></div>";
			endif;
}


// Widget Backend 
public function form( $instance ) {
	if ( isset( $instance[ 'title' ] ) ) {
		$title = $instance[ 'title' ];
	}
	else {
		$title = __( 'The Story widget title', 'diary_widget_domain' );
	}
	
	if ( isset( $instance[ 'page' ] ) ) {
		$page = $instance[ 'page' ];
	}
	else {
		$page = "";
	}

	// Widget admin form
	?>
	<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
	</p>
	<p>
		<label for="<?php echo $this->get_field_id( 'page' ); ?>"><?php _e( 'Page:' ); ?></label>
		<select class="widefat" id="<?php echo $this->get_field_id( 'page' ); ?>" name="<?php echo $this->get_field_name( 'page' ); ?>">
		<?php 
		$pages = get_pages();
		foreach($pages as $p):
			?>
			<option value="<?php echo $p->ID?>"<?php if($p->ID == esc_attr( $page )) echo " selected";?>><?php echo $p->post_title?></option>
			<?php 
		endforeach;
		?>
			
		</select>
	</p>
<?php 
}
	
// Updating widget replacing old instances with new
public function update( $new_instance, $old_instance ) {
	$instance = array();
	$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
	$instance['page'] = ( ! empty( $new_instance['page'] ) ) ? strip_tags( $new_instance['page'] ) : '';
return $instance;
}
} // Class diary_widget ends here

// Register and load the widget
function stories_load_widget() {
	register_widget( 'stories_widget' );
}
add_action( 'widgets_init', 'stories_load_widget' );