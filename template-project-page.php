<?php
/*
Template Name: פרוייקט הסיפורים
*/
get_header();

/* Start the Loop */
while ( have_posts() ) :
	the_post();
	get_template_part( '/template-parts/content/content-page' );
	$stories = get_posts(array('post_type' => 'stories_project',
				'orderby' => 'stories_project_id',
				'order' => 'ASC',
				'posts_per_page' => -1,
				'fields' => 'ids'));
	echo '<div id="stories">';
	echo '<ul class="storiesList">';
				foreach ($stories as $story):
					$title = get_the_title($story);
					$url = get_permalink($story);
					echo "<li><a href='$url'>";
					if(has_post_thumbnail( $story )) 
							print get_the_post_thumbnail($story, array(40,40));
					echo "<span>$title</span></a></li>";
				endforeach;
	echo '</ul>';
	echo '</div>';
	// If comments are open or there is at least one comment, load up the comment template.
		if ( comments_open() || get_comments_number() ) {
			comments_template();
		}
endwhile; // End of the loop.

get_sidebar('sidebar-1');
get_footer();
?>
<script defer>
(jQuery)(".entry-content").append((jQuery)("#stories").html());
(jQuery)("#stories").css("display","none");
</script>
